export const environment = {
  production: true,
  serverUrl: 'http://senseapp.space',
  apiUrl: 'http://api.senseapp.space',
  mqttHost: 'mqtt.senseapp.space',
  mqttPort: 8083,
  mqttPath: '/mqtt'
};
